package weather;

import java.util.Scanner;

public class WeatherApp {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите температуру: ");
        double temperature = scanner.nextInt();
        System.out.print("Введите скорость ветра(м/c): ");
        double windSpeed = scanner.nextDouble();
        System.out.print("Введите количество осадков(мм): ");
        double rainPowerInMillimeters = scanner.nextDouble();

        if(temperature<-10 || temperature>30 || windSpeed >6 || rainPowerInMillimeters > 1)
            System.out.println("Прогулка не рекомендуется");
        else
            System.out.println("Прогулка рекомендуется");
    }
}